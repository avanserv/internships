READ ME FOR EXPLORING RANSOMWARE - SupportingDocs
-------------------------------------------------
ExtractionCommands.txt
iptables rules		contains the iptables rules needed to make the internet connection on the guests work safely
jqFilterCommands.txt	contains the jq commands used to filter the data from JSON objects
links for cuckoo install	contains the links used to set up Cuckoo
usedCommands.txt	contains the commands used in the early stage of report analysis
Cuckoo set-up guide.txt	contains all info needed to set up cuckoo environment for dynamic analysis
readme.txt	is this file

