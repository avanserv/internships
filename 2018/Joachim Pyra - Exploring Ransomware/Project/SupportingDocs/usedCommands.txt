#Commands used during analysis
#-----------------------------

#COUNT OCCURENCE OF EVERY WORD IN A STRING OR FILE
$ echo "any text entry, any string, anything" | tr '[:space:]' '[\n*]' | grep -v "^\s*$" | sort | uniq -c | sort -bnr

#RENAME ALL FILES IN DIRECTORY (NON-RECURSIVE) TO START WITH STRING
$ rename 's/^/aString/' *
# FILENAME becomes aStringFILENAME

#REMOVE FIRST STRING FROM EVERY LINE
$ sed -e 's/^\w*\ *//' test.txt
#some file 1
#and text here
#more words after
# BECOMES
#file 1
#text here
#words after

#FIND SIMILAR LINES IN TWO FILES
$ fgrep -x -f file1 file2

#FIND DIFFERENT LINES IN TWO FILES
$ diff file1 file2

#FILTER LINES STARTING WITH SET OF STRINGS
$ grep -E '^(ATOM|CONECT|HETATM|TER|END)' test.txt


