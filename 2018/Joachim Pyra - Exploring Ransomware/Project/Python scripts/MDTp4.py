#!usr/bin/env python
import sys
import glob
import yara
from pathlib import Path
from numpy import zeros
import pefile
import time
import datetime

report = 0 #will have counter rows and n_func_impl columns
impl_func = ["YARA","pefile","cuckoo"]
n_func_impl = 3 #amount of functions that need to be implemented in report
counter = 1 #will contain amount of files to analyse
start_time = 0
stop_time = 0

def chooseOption(minV,maxV):
	inputValue = input('Choose: ')
	minValueOption = minV
	maxValueOption = maxV
	try:
		if int(inputValue) > maxValueOption or int(inputValue) < minValueOption:
			inputValue = 'a'
		val = int(inputValue)
	except ValueError:
		print('Invalid input')
		inputValue = (-1)
	return inputValue

def exit():
	print('Exiting application...')
	return

def fileAnalysis():
	print('running fileAnalysis')
	Matrix = listFilesInDir()
	option = (-1)
	while int(option) < 0:
		option = chooseOption(1,len(Matrix))
	global report
	report = [[0 for x in range(0,n_func_impl + 1)] for y in range(0,1)]
	global counter
	counter = 0 

	#starting timer
	global start_time
	start_time = time.time()

	analyse(Matrix,option)
	print_report(Matrix)
	return

def dirAnalysis():
	print('running dirAnalysis')
	Matrix = listFilesInDir()
	global counter
	
	#starting timer
	global start_time
	start_time = time.time()

	for x in range(0,len(Matrix)):
		counter = x
		analyse(Matrix,x)
	print_report(Matrix)
	return

def listFilesInDir():
	print('')
	#listed directory is directory where script is located
	i = 0
	for pth in Path.cwd().iterdir():
		if pth.suffix == '.exe':
			i += 1
			print(i),
			print(': '),
			print(pth.name)
			continue
		else:
			continue

	#add filenames to list Matrix
	Matrix = [0 for x in range(i)]
	#Matrix = [[0 for x in range(w)] for y in range(h)]
	j = -1
	for pth in Path.cwd().iterdir():
		if pth.suffix == '.exe':
			j += 1
			Matrix[j-i] = pth.name
			continue
		else:
			continue
	global report
	global counter
	counter = len(Matrix)
	report =[[0 for x in range(0,n_func_impl + 1)] for y in range(0,counter)]
	return Matrix

def analyse(Matrix,fileIndex):
	filename = Matrix[int(fileIndex)-1]
	global report
	global counter
	report[counter][0] = filename
	print('')
	print("Analysing "),
	print(filename)
	print((11 + len(filename)) * "-")
	cuckoo_analyse(filename)
	#yara_analyse(filename)
	#pefile_analyse(filename)
	return

def yara_analyse(filename):
	print('')
	print('Running YARA analysis...')
	rules = yara.compile(filepath='./rules/malwareRules.yara')
	matches = rules.match(data=filename)
	print(matches)
	if len(matches) > 0:
		global report
		global counter
		report[counter][1] += 1
	return

def pefile_analyse(filename):
	print('')
	print('Running PEfile analysis...')
	global report
	global counter
	try:
		pe = pefile.PE(filename)
		h1 = ['8:0','9:0','7:10','6:0','7:0','10:0','2:56','5:10','5:12','5:0','2:25','4:20','3:10']
		h2 = ['4:0','6:1','5:0','5:1','5:2','6:0']
		h3 = ['0:0','6:1','5:1','5:2','6:0','8:0','5:0','1:0','9:0','4:0','21315:20512','10:0','7:0','2:0','5:5','4:1','170:11','1:100','7200:700','4:73','6:2','4:50','0:1','3572:2','4:20']
		#algemeen pa_parameter_based(pe,'pe.WHICH_HEADER.feature',minimum,maximum,mode,hitrate)
		pa_parameter_based(pe,'pe.FILE_HEADER.NumberOfSections',1,9,4,3.64)
		pa_parameter_based(pe,'datetime.datetime.fromtimestamp(pe.FILE_HEADER.TimeDateStamp).strftime("%Y")',int(datetime.datetime.fromtimestamp(695692800).strftime('%Y')),int(datetime.datetime.fromtimestamp(time.time()).strftime('%Y')),4,12.05)	
		pa_parameter_based(pe,'str(pe.OPTIONAL_HEADER.MajorLinkerVersion)+":"+str(pe.OPTIONAL_HEADER.MajorLinkerVersion)',h1,-1,7,14.23)
		pa_parameter_based(pe,'str(pe.OPTIONAL_HEADER.MajorOperatingSystemVersion)+":"+str(pe.OPTIONAL_HEADER.MinorOperatingSystemVersion)',h2,-1,7,14.23)
		pa_parameter_based(pe,'str(pe.OPTIONAL_HEADER.MajorImageVersion)+":"+str(pe.OPTIONAL_HEADER.MinorImageVersion)',h3,-1,7,14.23)
		pa_parameter_based(pe,'pe.OPTIONAL_HEADER.NumberOfRvaAndSizes',16,16,4,2.16)
		
		#run for each sections
		for x in range(0,pe.FILE_HEADER.NumberOfSections):
			#extracting values
			SizeOfRawData = 'pe.sections[' + str(x) + '].SizeOfRawData'
			Misc_VirtualSize = 'pe.sections[' + str(x) + '].Misc_VirtualSize'
			PointerToLinenumbers = 'pe.sections[' + str(x) + '].PointerToLinenumbers'

			#making SizeOfRawData not 0
			if eval(SizeOfRawData) == 0:
				SizeOfRawData = str(int(eval(Misc_VirtualSize)) + 100)
			virt_by_raw = str(str(eval(Misc_VirtualSize))+"/"+str(eval(SizeOfRawData)))
			
			pa_parameter_based(pe,SizeOfRawData,0,-1,5,13.13)
			pa_parameter_based(pe,virt_by_raw,-1,10,2,3.22)
			pa_parameter_based(pe,PointerToLinenumbers,0,0,4,1.58)
	except (pefile.PEFormatError,AttributeError) as e:
		print("pe error")
		#print('{0:08b}'.format(pe.FILE_HEADER.Characteristics))
		return None

def pa_parameter_based(pe,parameter,min_value,max_value,mode,hitrate):
	# MODES (expressed as positive indication for malware)
	# min & max values always excluded
	par = eval(parameter)
	if par != 0:
		if mode == 1:
			#less than min
			try:
				if par < min_value:
					report[counter][2] += hitrate
			except:
				print("error on mode "),
				print(mode)
				pass
		elif mode == 2:
			#more than max
			try:
				if par > max_value:
					report[counter][2] += hitrate
			except:
				print("error on mode "),
				print(mode)
				pass
		elif mode == 3:
			#[min,max]
			try:
				if par > min_value and par < max_value:
					report[counter][2] += hitrate
			except:
				print("error on mode "),
				print(mode)
				pass		
		elif mode == 4:
			#![min,max]
			try:
				if par < min_value or par > max_value:
					report[counter][2] += hitrate
			except:
				print("error on mode "),
				print(mode)
		elif mode == 5:
			#equals min
			print(''),
		elif mode == 6:
			#parameter included in list (is positive indication of being malware)
			print(''),
		elif mode == 7:
			#parameter excluded from list (is positive indication of being malware)
			try:
				if par not in min_value:
					report[counter][2] += hitrate
			except:
				print("error on mode "),
				print(mode)
			
	return

def cuckoo_analysis(filename):
	
	return

def menu():
	'This is the functionality'
	print('')
	print('Menu')
	print('*****')
	print('0: exit')
	print('1: directory analysis')
	print('2: file analysis')
	global Matrix
	option = (-1)
	while int(option) < 0:
		option = chooseOption(0,2)
	if int(option) == 0:
		exit()
	elif int(option) == 1:
		dirAnalysis()
	elif int(option) == 2:
		fileAnalysis()
	else:
		print('Exiting due to unknown error')
	return

def print_report(Matrix):
	global counter
	global report
	global start_time
	elaps_time = time.time() - start_time
	print('')
	print('          ----------')
	print('          | REPORT |')
	print('          ----------')
	for j in range(0,len(report)):
		#iterate over each file
		print('')
		print(report[j][0]),
		print(': ')
		print((3 + len(report[j][0])) * "-")
		for i in range(1,len(report[0])):
			print(impl_func[i-1]),
			print(" gravity: "),
			print(report[j][i])
	print('')
	print('Elapsed time: '),
	print(round(elaps_time,2)),
	print(' seconds')
	return

#program starts here
print('Running Python version '),
print(sys.version)
print('Program version: 2.1')
print('     - YARA: recognises strings in filename')
print('     - pefile: implementation of parameter based header analysis')
print('     - reporting')

menu()

print('')
raw_input('Press any key to end')
print('Bye')