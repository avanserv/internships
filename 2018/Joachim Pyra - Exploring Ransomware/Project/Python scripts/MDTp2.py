#!usr/bin/env python
import sys
import glob
import yara
from pathlib import Path
from numpy import zeros
import pefile

report = 0 #will have counter rows and n_func_impl columns
impl_func = ["YARA","pefile"]
n_func_impl = 2 #amount of functions that need to be implemented in report
counter = 1 #will contain amount of files to analyse

def chooseOption(minV,maxV):
	inputValue = input('Choose: ')
	minValueOption = minV
	maxValueOption = maxV
	try:
		if int(inputValue) > maxValueOption or int(inputValue) < minValueOption:
			inputValue = 'a'
		val = int(inputValue)
	except ValueError:
		print('Invalid input')
		inputValue = (-1)
	return inputValue

def exit():
	print('Exiting application...')
	return

def fileAnalysis():
	print('running fileAnalysis')
	Matrix = listFilesInDir()
	option = (-1)
	while int(option) < 0:
		option = chooseOption(1,len(Matrix))
	global report
	report = [[0 for x in range(0,n_func_impl + 1)] for y in range(0,1)]
	global counter
	counter = 0 
	analyse(Matrix,option)
	print_report(Matrix)
	return

def dirAnalysis():
	print('running dirAnalysis')
	Matrix = listFilesInDir()
	global counter
	for x in range(0,len(Matrix)):
		counter = x
		analyse(Matrix,x)
	print_report(Matrix)
	return

def listFilesInDir():
	print('')
	#listed directory is directory where script is located
	i = 0
	for pth in Path.cwd().iterdir():
		if pth.suffix == '.exe':
			i += 1
			print(i),
			print(': '),
			print(pth.name)
			continue
		else:
			continue

	#add filenames to list Matrix
	Matrix = [0 for x in range(i)]
	#Matrix = [[0 for x in range(w)] for y in range(h)]
	j = -1
	for pth in Path.cwd().iterdir():
		if pth.suffix == '.exe':
			j += 1
			Matrix[j-i] = pth.name
			continue
		else:
			continue
	global report
	global counter
	counter = len(Matrix)
	report =[[0 for x in range(0,n_func_impl + 1)] for y in range(0,counter)]
	return Matrix

def analyse(Matrix,fileIndex):
	filename = Matrix[int(fileIndex)-1]
	global report
	global counter
	report[counter][0] = filename
	print('')
	print("Analysing "),
	print(filename)
	print((11 + len(filename)) * "-")
	yara_analyse(filename)
	pefile_analyse(filename)
	return

def yara_analyse(filename):
	print('')
	print('Running YARA analysis...')
	rules = yara.compile(filepath='./rules/malwareRules.yara')
	matches = rules.match(data=filename)
	print(matches)
	if len(matches) > 0:
		global report
		global counter
		report[counter][1] += 1
	return

def pefile_analyse(filename):
	print('')
	global report
	global counter
	try:
		pe = pefile.PE(filename)
		NumberOfSections = pe.FILE_HEADER.NumberOfSections
		print(type(pe))
		if NumberOfSections > 13:
			report[counter][2] += 3
		elif NumberOfSections < 5:
			report[counter][2] += 1
	except (pefile.PEFormatError,AttributeError) as e:
		return None
	return pe

def menu():
	'This is the functionality'
	print('')
	print('Menu')
	print('*****')
	print('0: exit')
	print('1: directory analysis')
	print('2: file analysis')
	global Matrix
	option = (-1)
	while int(option) < 0:
		option = chooseOption(0,2)
	if int(option) == 0:
		exit()
	elif int(option) == 1:
		dirAnalysis()
	elif int(option) == 2:
		fileAnalysis()
	else:
		print('Exiting due to unknown error')
	return

def print_report(Matrix):
	global counter
	global report 
	print('')
	print('          ----------')
	print('          | REPORT |')
	print('          ----------')
	for j in range(0,len(report)):
		#iterate over each file
		print('')
		print(report[j][0]),
		print(': ')
		print((3 + len(report[j][0])) * "-")
		for i in range(1,len(report[0])):
			print(impl_func[i-1]),
			print(" gravity: "),
			print(report[j][i])
	return

#program starts here
print('Running Python version '),
print(sys.version)
print('Program version: 1.1')
print('     - YARA: recognises strings in filename')
print('     - pefile: looks at NumberOfSections')
print('     - reporting')

menu()

print('')
raw_input('Press any key to end')
print('Bye')
