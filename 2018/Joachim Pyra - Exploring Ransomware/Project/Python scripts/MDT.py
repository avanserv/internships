#!usr/bin/env python
import sys
import os
import yara

print('Running Python version ',end='')
print(sys.version)
print('Program version: 1.0')
print('no added functionality')

def chooseOption(minV,maxV):
	inputValue = input('Choose: ')
	minValueOption = minV
	maxValueOption = maxV
	try:
		if int(inputValue) > maxValueOption or int(inputValue) < minValueOption:
			inputValue = 'a'
		val = int(inputValue)
	except ValueError:
		print('Invalid input')
		inputValue = (-1)
	return inputValue

def exit():
	print('Exiting application...')
	return

def fileAnalysis():
	print('running fileAnalysis')
	Matrix = listFilesInDir()
	option = (-1)
	while int(option) < 0:
		option = chooseOption(1,len(Matrix))
	analyse(Matrix,option)
	return

def dirAnalysis():
	print('running dirAnalysis')
	Matrix = listFilesInDir()
	for x in range(0,len(Matrix)):
		analyse(Matrix,x)
	return


def listFilesInDir():
	print('')
	#listed directory is directory where script is located
	directory = os.path.dirname(os.path.abspath(__file__))
	i = 0
	for file in os.listdir(directory):
		filename = os.fsdecode(file)
		if filename.endswith(".exe"):
			i += 1
			print(i,end='')
			print(': ',end='')
			print(filename)
			continue
		else:
			continue

	#add filenames to list Matrix
	Matrix = [0 for x in range(i)]
	#Matrix = [[0 for x in range(w)] for y in range(h)]
	j = -1
	for file in os.listdir(directory):
		filename = os.fsdecode(file)
		if filename.endswith(".exe"):
			j += 1
			Matrix[j-i] = filename
			continue
		else:
			continue
	return Matrix

def analyse(Matrix,fileIndex):
	print('')
	print(Matrix[int(fileIndex)-1])
	return

def main():
	'This is the functionality'
	print('Menu')
	print('*****')
	print('0: exit')
	print('1: directory analysis')
	print('2: file analysis')
	global Matrix
	option = (-1)
	while int(option) < 0:
		option = chooseOption(0,2)
	if int(option) == 0:
		exit()
	elif int(option) == 1:
		dirAnalysis()
	elif int(option) == 2:
		fileAnalysis()
	else:
		print('Exiting due to unknown error')
	return
main()
#jump to here when
print('')
input('Press any key to end')
