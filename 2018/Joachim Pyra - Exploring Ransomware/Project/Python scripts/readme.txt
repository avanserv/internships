READ ME FOR EXPLORING RANSOMWARE - Python scripts
-------------------------------------------------
MDT.py		header analysis script version 1: early version using latest Python version (3)
MDTp2.py	header analysis script version 2: early version using Python 2 due to compatibility issues
MDTp3.py	header analysis script version 3: implementation of parameter based header analysis
MDTp4.py	header analysis script version 4: latest version
readme.txt 	is this file
Yara/ 	contains everything needed to perform the YARA analysis part of the script (Yara has not been implemented in the latest version of MDT.py, being MDTp4.py)
