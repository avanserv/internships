READ ME FOR EXPLORING RANSOMWARE - FINDINGS
-------------------------------------------
FrequencyTable.txt: Frequency table with colums orignal line number, # occurences in benign samples, # occurences in ransomware samples, difference in # occurences in benign and ransomware samples, actual string
FrequencyTableAbs.txt # occurences in ransomware samples, difference in # occurences in benign and ransomware samples, actual string (same as FrequencyTalbe.txt)
FrequencyTableRel.txt # occurences in ransomware samples, relative comparison of # occurences in benign and ransomware samples, actual string
possibleRansomwareStrings.txt: list of strings used to generate FrequencyTable.txt
results.txt: manual written findings based on FrequencyTable.txt

