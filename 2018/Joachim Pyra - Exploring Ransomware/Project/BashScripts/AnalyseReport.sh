#!/usr/bin/env bash

if [ -n "$1" ] && [ -n "$2" ]
then
    echo "Using findings from file $1"
    echo "Analysing $2"
    rm findings.txt
    rm sorted.txt
    touch findings.txt
    touch sorted.txt
    f="findings.txt"
    s="sorted.txt"
    while read line; do
        echo "$line"
        grep "$line" "$2" >> findings.txt
    done <$1
    echo "Sorting..."
    cat findings.txt | uniq -c | sort -bnr > sorted.txt
    echo "Calculating score"

else
    echo "Use as following"
    echo "First argument is the list of findings"
    echo "Second argument is the unknown sample"
fi
