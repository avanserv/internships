#!/usr/bin/env bash

#iterate over all directories (datasets)
for dir in */ ; do
    echo "For directory $dir"

    #Making unique name for 'only' file
    only=$dir
    only=${dir::-1}
    only+=_only
    if [[ -e $only.txt ]] ; then
        i=0
        while [[ -e $only-$i.txt ]] ; do
            let i++
        done
        only=$only-$i
    fi
    echo "Creating $only.txt "
    touch "$only".txt

    #Making unique name for 'all' file
    name=$dir
    name=${dir::-1}
    name+=_all
    if [[ -e $name.txt ]] ; then
        i=0
        while [[ -e $name-$i.txt ]] ; do
            let i++
        done
        name=$name-$i
    fi
    echo "Creating $name.txt "
    touch "$name".txt

    #Make a file te collect top 30 api calls
    topAPI=$dir
    topAPI=${dir::-1}
    topAPI+=_topAPI
    if [[ -e $topAPI.txt ]] ; then
        i=0
        while [[ -e $topAPI-$i.txt ]] ; do
            let i++
        done
        topAPI=$topAPI-$i
    fi
    echo "Creating $topAPI.txt"
    touch "$topAPI".txt

    cd $dir
    #add all reports to one file
    cat *.json > ../"$name".txt

#[API] This part is specific for API calls
    echo "Collecting API data"
    #filter out 30 most used api calls
    cat ../"$name".txt | sort | uniq -c | sort -bnr | grep -E "api" | head -30 > ../"$topAPI".txt
    sed -e 's/^\w*\ *//' ../"$topAPI".txt | sed -e 's/^\w*\ *//' > ../"$only".txt
    rm ../"$topAPI".txt
    cat ../"$only".txt > ../"$topAPI".txt

    echo "Deleting $name.txt"

    #mv "$topAPI".txt ..
    #mv "$only".txt ..
    cd ..
    topAPIList+=($topAPI.txt)
    onlyList+=($only.txt)
#[API]
done

#[API] This part is specific for API calls

#Make a unique temp file
temp=temp
if [[ -e $temp.txt ]] ; then
    i=0
    while [[ -e $temp-$i.txt ]] ; do
        let i++
    done
    temp=$temp-$i
fi
touch "$temp".txt
c=0

for i in "${onlyList[@]}"
do
    d=0
    for j in "${topAPIList[@]}"
    do
        if [ $c != $d ]
        then
            diff "$i" "$j" > "$temp".txt
            rm "$i"
            touch "$i"
            grep -E "^(<)" "$temp".txt | sed 's/< *//' > "$i"
        fi
        d=$((d+1))
    done
    c=$((c+1))
done
#[API]
