#!/usr/bin/env bash
# make sure jq & curl are installed using
# sudo apt-get install curl
# sudo apt-get install jq 
#submit file (TD: add checks to see if file is compatible)
for dir in */ ; do
	path=./"$dir"
	cd "$path"
	echo "Analysing all files in $dir"
	for file in * ; do
		if [[ $file != *.json ]]
		then
			wf=$(pwd)/"$file"
			id=$(curl -s -F file=@"$wf" http://localhost:8090/tasks/create/file | jq -r '.task_id')
			echo "Building report for $file with id $id"

			#request report based on id
			ready="$(curl -s http://localhost:8090/tasks/view/"$id" | jq -r '.task.status')"
			echo "Waiting for report..."
			while [ "$ready" != "reported" ]; do
				echo -ne "Status: $ready \r"
				sleep 1
				echo -ne "Status: $ready. \r"
				sleep 1
				echo -ne "Status: $ready.. \r"
				sleep 1
				echo -ne "Status: $ready... \r"
				sleep 1
				ready="$(curl -s http://localhost:8090/tasks/view/"$id" | jq -r '.task.status')"
			done
			echo -ne '\n'
			echo $(curl -s http://localhost:8090/tasks/report/$id > "$file".json)
		fi
	done
	echo "All files in $dir analysed"
	cd ..
done
