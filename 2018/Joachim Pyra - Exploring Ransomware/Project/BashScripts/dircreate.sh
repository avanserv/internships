#!/usr/bin/env bash
function createUnique {
    only=$dir
    only=${dir::-1}
    only+=_only
    if [[ -e $only.txt ]] ; then
        i=0
        while [[ -e $only-$i.txt ]] ; do
            let i++
        done
        only=$only-$i
    fi
    echo "Creating $only.txt "
    touch "$only".txt
}