#!/usr/bin/env bash
# cat Ransomware_all.txt | sed "s/^[ \t]*//" | sort | uniq > Ransomware_oneLine.txt
# cat Benign_all.txt | sed "s/^[ \t]*//" | sort | uniq > Benign_oneLine.txt
# comm -1 -2 Benign_oneLine.txt Ransomware_oneLine.txt > comm.txt
# cat Ransomware_all.txt | sed "s/^[ \t]*//" | sort > Ransomware_noLead.txt
# cat Benign_all.txt | sed "s/^[ \t]*//" | sort > Benign_noLead.txt
#re
#comm -23 Ransomware_noLead.txt Benign_noLead.txt > Ransomware_Uniq.txt
#comm -23 Benign_noLead.txt Ransomware_noLead.txt > Benign_Uniq.txt
#comm -23 Benign_noLead.txt Benign_Uniq.txt > Benign_noUniq.txt
#comm -23 Ransomware_noLead.txt Ransomware_Uniq.txt > Ransomware_noUniq.txt
#cat Benign_noUniq.txt | uniq -c > Benign_noUniqCounted.txt
#cat Ransomware_noUniq.txt | uniq -c > Ransomware_noUniqCounted.txt
#cat Benign_noUniq.txt | uniq > Benign_noUniqSorted.txt
#cat Ransomware_noUniq.txt | uniq > Ransomware_noUniqSorted.txt
#re

#BUILD ALL FROM EQUAL AMOUNT (B&R) -> execute rest

#cat Ransomware_all.txt | sed "s/^[ \t]*//" | sort > Ransomware_noLead.txt
#cat Benign_all.txt | sed "s/^[ \t]*//" | sort > Benign_noLead.txt
#grep -Fx -f comm.txt Ransomware_noLead.txt > Ransomware_new.txt
#grep -Fx -f comm.txt Benign_noLead.txt > Benign_new.txt
#cat Benign_new.txt | uniq -c > Benign_def.txt
#cat Ransomware_new.txt | uniq -c > Ransomware_def.txt
#cat Benign_new.txt | uniq > Benign_wor.txt
#cat Ransomware_new.txt | uniq > Ransomware_wor.txt

linesAmount=$(cat Ransomware_def.txt | wc -l)
counter=1
rm FrequencyTable.txt
while [ "$counter" -le "$linesAmount" ]
do
	lineRans=$(sed -n "$counter"p Ransomware_def.txt | sed 's/^[ \t]*//' | grep -Eo '^[^ ]+')
	lineBeni=$(sed -n "$counter"p Benign_def.txt | sed 's/^[ \t]*//' | grep -Eo '^[^ ]+')
	diffr="$((lineRans - lineBeni))"
	if [ $diffr -ne "0" ]
	then
		echo -e "$counter\t$lineRans\t$lineBeni\t$diffr" >> FrequencyTable.txt
	fi
	let counter++
done
sort -t$'\t' -k4 -nr FrequencyTable.txt -o FrequencyTable.txt 

counter=1
while [ "$counter" -le "$linesAmount" ]
do
	id=$(sed -n "$counter"p FrequencyTable.txt | cut -f1)
	
	wrd=$(sed -n "$id"p Ransomware_def.txt | sed 's/^[ \t]*//' | sed -e 's/^\w*\ *//')
	sed -i -E "$counter"'s/$/\t'"$wrd"'/' FrequencyTable.txt
	let counter++
done
