#!/usr/bin/env bash
# make sure jq & curl are installed using
# sudo apt-get install curl
# sudo apt-get install jq 
if [ -n "$1" ]
then
    if [ ! -f ./$1 ]; then
        echo "Make sure the file exists in the current directory"
    else
	#submit file (TD: add checks to see if file is compatible)
	path=$(pwd)/"$1"
	echo "$path"
	id=$(curl -s -F file=@"$path" http://localhost:8090/tasks/create/file | jq -r '.task_id')
	echo "Building report for $1 with id $id"

	#request report based on id
	ready="$(curl -s http://localhost:8090/tasks/view/"$id" | jq -r '.task.status')"
	echo "Waiting for report..."
	while [ "$ready" != "reported" ]; do
		echo -ne "Status: $ready \r"
		sleep 1
		echo -ne "Status: $ready. \r"
		sleep 1
		echo -ne "Status: $ready.. \r"
		sleep 1
		echo -ne "Status: $ready... \r"
		sleep 1
		ready="$(curl -s http://localhost:8090/tasks/view/"$id" | jq -r '.task.status')"
	done
	echo -ne '\n'
	echo "Calculating score for $1"
	echo $(curl -s http://localhost:8090/tasks/report/$id | jq '.info.score')
    fi
else
    echo "Enter a file as parameter"
fi
