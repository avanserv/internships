#!/usr/bin/env bash

#iterate over all directories (datasets)
for dir in */ ; do
    echo "For directory $dir"

    #Making 'only' file
    only=$dir
    only=${dir::-1}
    only+=_only
    echo "Creating $only.txt "
    rm "$only".txt
    touch "$only".txt

    #Making 'all' file
    name=$dir
    name=${dir::-1}
    name+=_all
    echo "Creating $name.txt "
    rm "$name".txt
    touch "$name".txt

    #Make a file te collect findings
    topFind=$dir
    topFind=${dir::-1}
    topFind+=_topFind
    echo "Creating $topFind.txt"
    rm "$topFind".txt
    touch "$topFind".txt

    #Making scoretable
    scrtbl=$dir
    scrtbl=${dir::-1}
    scrtbl+=_scoreTable
    echo "Creating $scrtbl.txt "
    rm "$scrtbl".txt
    touch "$scrtbl".txt

    cd $dir
    #add all reports to one file
    cat *.json > ../"$name".txt

    echo "Collecting data"
    #filter out
    #cat  | sed "s/^[ \t]*//" | sort | uniq -c | sort -bnr | head -150 | sed "s/^[ \t]*//" > ../"$scrtbl".txt
    cat ../"$name".txt | sed "s/^[ \t]*//" | sort | uniq -c | sort -bnr | head -150 | sed "s/^[ \t]*//" | sed 's/^\w*\ *//' | sort > ../"$topFind".txt
    cat ../"$topFind".txt > ../"$only".txt
    #rm ../"$topFind".txt
    #cat ../"$only".txt > ../"$topFind".txt

    #Creating scoretable
    tot=($(ls | wc -l))
    let "tot=tot+1"
    occ=0
    val=0
    lin=0
    #score weight:
    while read line; do
        val=($(grep -Fx "$line" ./*.json | uniq | wc -l))
        let "occ=occ+val"
        let "lin=lin+1"
    done < ../"$only".txt
    lin=$((lin*tot))
    score=$((1000*occ/lin))
    echo "$occ"
    echo "$lin"
    echo "$score"
    echo "Done"
    cd ..
    topFindList+=($topFind.txt)
    onlyList+=($only.txt)
done

#Make a unique temp file
temp=temp
rm "$temp".txt
touch "$temp".txt
c=0

for i in "${onlyList[@]}"
do
    d=0
    for j in "${topFindList[@]}"
    do
        if [ $c != $d ]
        then
            diff "$i" "$j" > "$temp".txt
            rm "$i"
            touch "$i"
            grep -E "^(<)" "$temp".txt | sed 's/< *//' > "$i"
        fi
        d=$((d+1))
    done
    c=$((c+1))
done