#!/usr/bin/env bash
# sudo apt-get install jq
echo "API calls used"
while read line; do
	if [[ "$line" != *'{'* ]] && [[ "$line" != *'}'* ]] && [[ -n "$line" ]]
	then
		if [[ "${line: -1}" == "," ]]
		then
			echo -ne "\\t"
			grep -oP '(?<=").*?(?=")' <<< "$line" | tr '\n' ':  '
			grep -oP '(?<= ).*?(?=,)' <<< "$line"
		else
			echo -ne "\\t"
			grep -oP '(?<=").*?(?=")' <<< "$line" | tr '\n' ':  '
			grep -oP '(?<= ).*?(?=$)' <<< "$line"
		fi
	fi
done < <(jq -r '.behavior.apistats[]' $1)

writeOps=$(jq -r '.behavior.summary.file_written | length' $1)
echo ""
echo "Files written: $writeOps"

dirListed=$(jq -r '.behavior.summary.directory_enumerated | length' $1)
echo ""
echo "Directories enumerated: $dirListed"

echo ""
echo "Alerts"
jq -r '.signatures[] | select(.severity | contains(2,3,4,5,6)) | "\tNAME:\t\t" + .name + "\n\tDESCRIPTION:\t" + .description + "\n\tSEVERITY:\t" + (.severity | tostring) + "\n\tMARKCOUNT:\t" + (.markcount | tostring) + "\n\n"' $1
