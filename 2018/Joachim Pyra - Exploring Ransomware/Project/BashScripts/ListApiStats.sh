#!/usr/bin/env bash
# sudo apt-get install jq 
jq -r '.behavior.apistats[]' $1 > tempapi.txt
while read line; do
	if [[ "$line" != *'{'* ]] && [[ "$line" != *'}'* ]] && [[ -n "$line" ]]
	then
		if [[ "${line: -1}" == "," ]]
		then
			name=grep -oP '(?<=").*?(?=")' <<< "$line"
			value=grep -oP '(?<= ).*?(?=,)' <<< "$line"
		else
			name=grep -oP '(?<=").*?(?=")' <<< "$line"
			value=grep -oP '(?<= ).*?(?=$)' <<< "$line"
		fi
	fi	
done <tempapi.txt
rm tempapi.txt

#grep -oP '(?<=").*?(?=")' tempapi2.txt

