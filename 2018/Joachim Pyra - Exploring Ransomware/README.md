READ ME FOR EXPLORING RANSOMWARE
================================

**Author:** Joachim pyra<br>
**Mentor:** Dario Incalza<br>
**Intership period:** 05/02/2018 - 11/05/2018<br>
**Location:** EY Diegem, Belgium<br>
**Subject:** Exploring ransomware is a project researching ransomware. With the included tools, static and dynamic analysis can be performed. In order for the tools to work, a Cuckoo sandbox environment has to be set up. (Guide: /SupporingDocs/Cuckoo set-up guide.txt)

Content
-------
`Bash Scripts/` contains all scripts used for analysis<br>
`Findings/` contains findings extracted from the data and reports<br>
`Python Scripts/` contains the Python scripts used for static header analysis<br>
`Supporting Docs/` contains manuals and documentation for rebuilding or expanding the project<br>
`README.md` is this file

Terminology
-----------
Reports are generated using the Cuckoo Sandbox<br>
Data is generated using the scripts

Notes
-----
For security reasons, the malware samples are not present. These can be downloaded from https://github.com/ytisf/theZoo.<br>
ATTENTION: These are active malware samples and could possibly infect your computer if not handled properly.

