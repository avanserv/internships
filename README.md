# Repository Folder Structure

**Year of the internship**<br>
└───**Intern Name - Internship Title**<br>
	├───**Documentation**<br>
	│ 	└───*Your documentation and the results of your researches go here.*<br>
	├───**Presentation**<br>
	│ 	└───*The resources used for your final presentation go here. This include*<br>
	│		*your slides and othe resources used during the presentation.*<br>
	├───**Project**<br>
	│ 	└───*Whatever you did for the internship (technical) goes here.*<br>
	│		*Include what you created, not the tools you downloaded.*<br>
	│		*Do not hesitate to create sub-directories to organize your work.*<br>
	│		*Also, do not include the output of the tools you used if it is huge*<br>
	│		*and easily reproducible. Document configuration and commands instead.*<br>
	└───README.md *(With all relevant information)*

**Make sure to not overwrite others work!**
